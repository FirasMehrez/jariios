//
//  HoodEntity.swift
//  JariIOS
//
//  Created by Firas mehrez on 26/06/2020.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import Foundation


struct UserEntity : Decodable,Identifiable{
    var id : Int
    var username : String
    var birthDate : String
    var phoneNumber:String
    var picture:String
    var email: String
    var longitude : Float
    var lattitude : Float
//    var password: String
//      var role: String
//      var address : String
//    var createdAt : String
//    var updatedAt : String
//
    
  /*  init(json: [String: Any]) {
        id = json["id"] as? Int ?? -1
        type = json["type"] as? String ?? ""
        place = json["place"] as? String ?? ""
        description = json["description"] as? String ?? ""
        url_image = json["url_image"] as? String ?? ""
        id_user = json["id_user"] as? Int ?? -1
        lost = json["lost"] as? Int ?? -1
        found = json["found"] as? Int ?? -1
        date = json["date"] as? String ?? ""
        
    }*/
    enum CodingKeys: String, CodingKey {
             case id = "id"
             case username = "username"
             case birthDate = "birthDate"
             case phoneNumber = "phoneNumber"
             case picture = "picture"
             case longitude = "longitude"
             case lattitude = "latitude"
             case email = "email"
     
          }
    
}
