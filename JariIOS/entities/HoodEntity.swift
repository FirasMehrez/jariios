//
//  HoodEntity.swift
//  JariIOS
//
//  Created by Firas mehrez on 26/06/2020.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import Foundation


struct HoodEntity : Decodable,Identifiable{
    var id : Int
    var type : String
    var title : String
    var content: String
    var picture : String
    var longitude : Float
    var lattitude : Float
    var date : String
    var user : String
    
    
  /*  init(json: [String: Any]) {
        id = json["id"] as? Int ?? -1
        type = json["type"] as? String ?? ""
        place = json["place"] as? String ?? ""
        description = json["description"] as? String ?? ""
        url_image = json["url_image"] as? String ?? ""
        id_user = json["id_user"] as? Int ?? -1
        lost = json["lost"] as? Int ?? -1
        found = json["found"] as? Int ?? -1
        date = json["date"] as? String ?? ""
        
    }*/
    enum CodingKeys: String, CodingKey {
             case id = "hood_id"
             case type = "hood_type"
             case title = "hood_title"
             case content = "hood_content"
             case picture = "hood_picture"
             case longitude = "hood_longitude"
             case lattitude = "hood_latitude"
             case date = "hood_date"
             case user = "user_username"
     
          }
    
}
