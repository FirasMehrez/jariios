//
//  HoodView.swift
//  JariIOS
//
//  Created by Firas mehrez on 26/06/2020.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import SwiftUI
import Foundation
import Combine

struct HoodView: View {
    var body: some View {
     Hood()
    }
}

struct HoodView_Previews: PreviewProvider {
    static var previews: some View {
        HoodView()
    }
}

public class getHood: ObservableObject {

    @Published var hoods = [HoodEntity]()
  
    init(){
        load()
    }
    
    
    func getValue(forKey key : String)->String? {
         return Bundle.main.infoDictionary?[key] as? String
     }
    
    func load() {
          
         let parameters = ["long": 32, "latt": 32]
         
         //_ = UserDefaults.standard.set(data, forKey: "token") // FOR TOKEn
         guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/hood") else { return }
         var request = URLRequest(url: url)
         request.httpMethod = "POST"
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")

         // HTTPBODY for send request
         guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
         request.httpBody = httpBody
         let session = URLSession.shared
         session.dataTask(with: request) { (data, response, error) in
             do {
                         if let d = data {
                             let decodedLists = try JSONDecoder().decode([HoodEntity].self, from: d)
                             DispatchQueue.main.async {
                                 self.hoods = decodedLists
                             }
                         }else {
                             print("No Data")
                         }
                     } catch  {
                         print (error)
                     }
         }.resume()
         
    }
}


struct Hood : View {
    
    @State var menu = 0
    @State var page = 0
    
    var body: some View{
        
        ZStack{
            
            Color("Color-1").edgesIgnoringSafeArea(.all)
            
            
            VStack{
                
                VStack{
                    
                    
                    Text("Hood")
                        .font(.system(size: 22))
                        .fontWeight(.bold)
               
                   // FloatingLFView()
                }
            
                .padding(.top,5)
                
                GeometryReader{g in
                    
                    // Simple Carousel List....
                    // Using GeomtryReader For Getting Remaining Height...
                    
                    CarouselLost(width: UIScreen.main.bounds.width, page: self.$page, height: g.frame(in: .global).height)
                }
                
                PageControl(page: self.$page)
                .padding(.top, 10)
            }
            .padding(.vertical)
        }
    }
}


struct List : View {
    
    @Binding var page : Int
    @ObservedObject var fetcher = getHood()
    
    var body: some View{
        
        HStack(spacing: 0){
            
            ForEach(fetcher.hoods){i in
                
                CardHood(page: self.$page, width: UIScreen.main.bounds.width, data: i)
            }
        }
    }
}

struct CardHood : View {
    
    @Binding var page : Int
    var width : CGFloat
    var data : HoodEntity
 
    
    var body: some View{
        
        VStack{
            
            VStack{
                
                Text(self.data.type)
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.top,20)
              
                Text(self.data.date)
                 .foregroundColor(.gray)
                 .padding(.vertical)
                              
                              
                
                Spacer(minLength: 0)
                 (self.data.type == "Cat" ?
                Image("kiki1")
                .resizable()
                .frame(width: self.width - (self.page == self.data.id ? 100 : 150), height: (self.page == self.data.id ? 250 : 200))
                    .cornerRadius(20)
                    :
                    Image("hood")
                                 .resizable()
                                 .frame(width: self.width - (self.page == self.data.id ? 100 : 150), height: (self.page == self.data.id ? 250 : 200))
                                     .cornerRadius(20)
                )
                
                Text(self.data.content)
                    .font(.title)
                    .padding(.top, 10)
                Text(self.data.content)
                                 .font(.body)
                                 .padding(.top, 2)
                
                Button(action: {
                    
                }) {
                    
                    Text(self.data.title)
                        .foregroundColor(.black)
                        .padding(.vertical, 10)
                        .padding(.horizontal, 30)
                }
                .background(Color("Color"))
                .clipShape(Capsule())
                .padding(.top, 20)
                
                
                Spacer(minLength: 0)
                
                // For Filling Empty Space....
            }
            .padding(.horizontal, 20)
            .padding(.vertical, 25)
            .background(Color.white)
            .cornerRadius(20)
            .padding(.top, 25)
            .padding(.vertical, self.page == data.id ? 0 : 25)
            .padding(.horizontal, self.page == data.id ? 0 : 25)
            
            // Increasing Height And Width If Currnet Page Appears...
        }
       .frame(width: self.width)
        .animation(.default)
    }
}

struct CarouselLost : UIViewRepresentable {
    
    
    func makeCoordinator() -> Coordinator {
        
        return CarouselLost.Coordinator(parent1: self)
    }
    
    var width : CGFloat
    @Binding var page : Int
    @ObservedObject var fetcher = getHood()
    var height : CGFloat
    
    func makeUIView(context: Context) -> UIScrollView{
        
        // ScrollView Content Size...
        
        let total = width * CGFloat(3)
        let view = UIScrollView()
        view.isPagingEnabled = true
        //1.0  For Disabling Vertical Scroll....
        view.contentSize = CGSize(width: total, height: 1.0)
        view.bounces = true
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.delegate = context.coordinator
        
        // Now Going to  embed swiftUI View Into UIView...
        
        let view1 = UIHostingController(rootView: List(page: self.$page))
        view1.view.frame = CGRect(x: 0, y: 0, width: total, height: self.height)
        view1.view.backgroundColor = .clear
        
        view.addSubview(view1.view)
        
        return view
        
    }
    
    func updateUIView(_ uiView: UIScrollView, context: Context) {
        
        
    }
    
    class Coordinator : NSObject,UIScrollViewDelegate{
        
        
        var parent : CarouselLost
        
        init(parent1: CarouselLost) {
            
        
            parent = parent1
        }
        
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            
            // Using This Function For Getting Currnet Page
            // Follow Me...
            
            let page = Int(scrollView.contentOffset.x / UIScreen.main.bounds.width)
            
            self.parent.page = page
        }
    }
}

// Now We GOing TO Implement UIPageControl...

struct PageControl : UIViewRepresentable {
    
    @Binding var page : Int
    @ObservedObject var fetcher = getHood()
    
    func makeUIView(context: Context) -> UIPageControl {
        
        let view = UIPageControl()
        view.currentPageIndicatorTintColor = .black
        view.pageIndicatorTintColor = UIColor.black.withAlphaComponent(0.2)
        view.numberOfPages = 5
        print(fetcher.hoods.count)
        return view
    }
    
    func updateUIView(_ uiView: UIPageControl, context: Context) {
        
        // Updating Page Indicator When Ever Page Changes....
        
        DispatchQueue.main.async {
            
            uiView.currentPage = self.page
        }
    }
}

