//
//  HoodButtonControllerView.swift
//  JariIOS
//
//  Created by Sadok Laouissi on 6/24/20.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import SwiftUI

struct HoodButtonControllerView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct HoodButtonControllerView_Previews: PreviewProvider {
    static var previews: some View {
        HoodButtonControllerView()
    }
}
