//
//  BottomBarUIView.swift
//  JariIOS
//
//  Created by Firas mehrez on 26/06/2020.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import SwiftUI

struct BottomBarUIView: View {
    var body: some View {
     /*  VStack{
             Button(action: {
                 self.isDisconnected = true
             }) {
                 Text("Disconnect")
             }.alert(isPresented: $isDisconnected, content: {
                 Alert(title: Text("Disconnect"), message: Text("Are you sure ?"), primaryButton: .destructive(Text("Yes"), action: {
                     disconnectFromCoreData()
                     self.mode.wrappedValue.dismiss()
                     GIDSignIn.sharedInstance()?.disconnect()
                 }), secondaryButton: .cancel(Text("No")))
             })
             NavigationLink(destination:  SignInUpView(), isActive: self.$isDisconnected) {
                 Text("")
             }
         }.navigationBarBackButtonHidden(true)*/
        
                    
                    TabView()
                        .navigationBarTitle("",displayMode: .inline)
                        .navigationBarHidden(true)
                        .navigationBarBackButtonHidden(true)
          
     }
    
}

struct BottomBarUIView_Previews: PreviewProvider {
    static var previews: some View {
        BottomBarUIView()
    }
}

struct TabView : View {
     
     @State var index = 0
    // @Environment(.colorScheme) var scheme

   
     var body: some View{
         
         VStack(spacing: 0){
             
             ZStack{
              HoodView()
                                  .opacity(self.index == 0 ? 1 : 0)
                
                                     
                 Account()
                     .opacity(self.index == 3 ? 1 : 0)
                 
             }
             
             HStack{
                 
                 Button(action: {
                     
                     self.index = 0
                     
                 }) {
                     
                     HStack(spacing: 6){
                      
                         Image("home")
                             // dark mode adoption...
                             .foregroundColor(self.index == 0 ? .white : .primary)
                         
                         if self.index == 0{
                             
                             Text("Hood")
                                 .foregroundColor(.white)
                         }
                         
                     }
                     .padding(.vertical,10)
                     .padding(.horizontal)
                     .background(self.index == 0 ? Color("Color") : Color.clear)
                     .clipShape(Capsule())
                 }
                 
                 Spacer(minLength: 0)
                 
                 Button(action: {
                     
                     self.index = 1
                     
                 }) {
                     
                     HStack(spacing: 6){
                      
                         Image("search")
                             // dark mode adoption...
                             .foregroundColor(self.index == 1 ? .white : .primary)
                         
                         if self.index == 1{
                             
                             Text("Lost&Found")
                                 .foregroundColor(.white)
                         }
                         
                     }
                     .padding(.vertical,10)
                     .padding(.horizontal)
                     .background(self.index == 1 ? Color("Color") : Color.clear)
                     .clipShape(Capsule())
                 }
                 
                 Spacer(minLength: 0)
                 
                 Button(action: {
                     
                     self.index = 2
                     
                 }) {
                     
                     HStack(spacing: 6){
                      
                         Image("list")
                             // dark mode adoption...
                             .foregroundColor(self.index == 2 ? .white : .primary)
                         
                         if self.index == 2{
                             
                             Text("Add")
                                 .foregroundColor(.white)
                         }
                         
                     }
                     .padding(.vertical,10)
                     .padding(.horizontal)
                     .background(self.index == 2 ? Color("Color") : Color.clear)
                     .clipShape(Capsule())
                 }
                 
                 Spacer(minLength: 0)
                 
                 Button(action: {
                     
                     self.index = 3
                     
                 }) {
                     
                     HStack(spacing: 6){
                      
                         Image("person")
                             // dark mode adoption...
                             .foregroundColor(self.index == 3 ? .white : .primary)
                         
                         if self.index == 3{
                             
                             Text("Profile")
                                 .foregroundColor(.white)
                         }
                         
                     }
                     .padding(.vertical,10)
                     .padding(.horizontal)
                     .background(self.index == 3 ? Color("Color") : Color.clear)
                     .clipShape(Capsule())
                 }
             }
             .padding(.horizontal,25)
             .padding(.top)
             // based on device bottom padding will be changed...
             .padding(.bottom,UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0 ? 10 : UIApplication.shared.windows.first?.safeAreaInsets.bottom)
             // for shadow...
             /*.background(self.scheme == .dark ? Color.black : Color.white)*/
                  .background(Color.white)
             .shadow(color: Color.primary.opacity(0.08), radius: 5, x: 5, y: -5)
         }
         .edgesIgnoringSafeArea(.bottom)
     }
 }
 
 
 struct Account : View {
     
     var body: some View{
         
         VStack{
             
             Text("Account")
         }
         .frame(maxWidth: .infinity, maxHeight: .infinity)
     }
 }



/*func disconnectFromCoreData() {
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
    request.returnsObjectsAsFaults = false
    
    do{
        let results = try context.fetch(request)
        if results.count > 0 {
            for r in results as! [NSManagedObject] {
                context.delete(r)
                do{
                    try context.save()
                }catch{
                    print("ERROR saving deleting user from core data")
                }
                
                print("User deleted from core data")
            }
        }else{
            print("No user in core data")
        }
    }catch{
        print("ERROR deleting user from core data")
    }
}*/
