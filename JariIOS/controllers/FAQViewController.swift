//
//  FAQViewController.swift
//  JariIOS
//
//  Created by Sadok Laouissi on 6/28/20.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import UIKit


class FAQViewController: UIViewController {

    var faqView: FAQView!
    override func viewDidLoad() {
        super.viewDidLoad();
        let rect = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        let items = [FAQItem(question: "What is Jari?", answer: "Jari is a new app for what's new and popular in your local area as well as the losts and founds."),
                    FAQItem(question: "What are the types of Announce in the hood?", answer: "A Hood Announce can be an invite to an event, a party , one of your neigbhour need help... etc"),
                    FAQItem(question: "How do I report a lost item?", answer: "The easiest and most efficient way to report a lost item is our online Lost and Found form. Once submitted, all of your lost item information will be accessible by everyone. You will receive a phone call from the person who find it. And also keep track on the found items in your last known location area.")]

        let faqView = FAQView(frame: rect, title: "Top Queries", items: items)
        view.addSubview(faqView)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
