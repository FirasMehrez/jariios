//
//  LostListViewController.swift
//  JariIOS
//
//  Created by Sadok Laouissi on 6/28/20.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import UIKit
import JWTDecode
class LostListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    let films = ["Bad Boys For Life","DoLittle","The gentlemen","the Grudge","underwater"]
    var losts = [LostEntity]()
     var username:String? = ""
    
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return losts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lost")
        let content = cell?.viewWithTag(0)
        let labeltitle = content?.viewWithTag(1) as! UILabel
        let labeluser = content?.viewWithTag(3) as! UILabel
        let labeltype = content?.viewWithTag(4) as! UILabel
        let image = content?.viewWithTag(2) as! UIImageView
        labeltitle.text = losts[indexPath.row].name
        labeluser.text = losts[indexPath.row].user
        labeltype.text = losts[indexPath.row].type
        
        
        let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/images/"+losts[indexPath.row].picture)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        image.image = UIImage(data: data!)
        
        
        //labeluser.text = hoods[indexPath.row].user
        return cell!
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            if self.losts[indexPath.row].user == self.username!
            {
                self.deleteLost(id: "\(self.losts[indexPath.row].id)")
                self.losts.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                let alertController = UIAlertController(title: "Succes", message:
                    "Annonce has been Deleted" + ", Thanks", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                let alertController = UIAlertController(title: "Fail", message:
                    "You cannot delete other annoncements" + ", Thanks", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
            
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    override func viewDidLoad() {
        
        let defaults = UserDefaults.standard
        let  token : String?     = defaults.string(forKey: "token")
        print (token)
        do {
            let jwt = try decode(jwt: token!)
            let claim = jwt.claim(name: "username")
            username = claim.string
            print("Username in jwt was \(username)")
            
        } catch let error as NSError {
            error.localizedDescription
        }
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          
          if segue.identifier == "toLostDetail"
          {
              let indexPath = sender as! IndexPath
              let lost = losts[indexPath.row]
              let dvc  = segue.destination as! DetailsLostViewController
             print (lost)
              dvc.titles = lost.name
              dvc.content = lost.description
              dvc.picture = lost.picture
              dvc.date = lost.date
              dvc.user = lost.user
              dvc.type = lost.type
              dvc.longitude = lost.longitude
              dvc.latitude = lost.lattitude
          }
          
          
      }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           performSegue(withIdentifier: "toLostDetail", sender: indexPath)
           
       }
    
    func deleteLost(id:String) {
        
        
        
        //_ = UserDefaults.standard.set(data, forKey: "token") // FOR TOKEn
        guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/Lost/"+id) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        // HTTPBODY for send request
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            do {
                
            } catch  {
                print (error)
            }
        }.resume()
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
