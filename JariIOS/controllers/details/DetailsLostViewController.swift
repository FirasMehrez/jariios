//
//  DetailsLostViewController.swift
//  JariIOS
//
//  Created by Sadok Laouissi on 6/28/20.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import UIKit
import MapKit

class DetailsLostViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var labeluser: UILabel!
    @IBOutlet weak var labeldate: UILabel!
    @IBOutlet weak var labeltype: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var labeltitles: UILabel!
    
    @IBOutlet weak var textdesc: UITextView!
    var picture:String?
    var titles:String?
    var content:String?
    var longitude:Float?
    var latitude:Float?
    var date :String?
    var user :String?
    var type :String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labeltype.text = type!
        textdesc.text = content!
        labeluser.text =  user!
        labeltitles.text = titles!
        let index = date!.index(date!.startIndex, offsetBy: 10)
        let datedate = date![..<index]
        
        let initialLocation = CLLocation(latitude: CLLocationDegrees(latitude!), longitude: CLLocationDegrees(longitude!))
        map.centerToLocation    (initialLocation)
        
        
        
        labeldate.text = String(datedate)
        
        
        
        let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/images/"+picture!)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        imageView.image = UIImage(data: data!)
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

private extension MKMapView {
    func centerToLocation(
        _ location: CLLocation,
        regionRadius: CLLocationDistance = 1000
    ) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}
