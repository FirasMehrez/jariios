//
//  SecondViewController.swift
//  JariIOS
//
//  Created by Firas mehrez on 27/04/2020.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import UIKit
import CoreLocation
import JWTDecode

class HoodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    let films = ["Bad Boys For Life","DoLittle","The gentlemen","the Grudge","underwater"]
    var hoods = [HoodEntity]()
    var longitude:Double = 0.0
    var lattitude:Double = 0.0
    var username:String? = ""
    private var locationManager:CLLocationManager?
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(hoods.count)
        return hoods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print (hoods)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "hood")
        let content = cell?.viewWithTag(0)
        let labeltitle = content?.viewWithTag(1) as! UILabel
        let labeluser = content?.viewWithTag(3) as! UILabel
        
        let labelType = content?.viewWithTag(4) as! UILabel
        let image = content?.viewWithTag(2) as! UIImageView
        labeltitle.text = hoods[indexPath.row].title
        labeluser.text = hoods[indexPath.row].user
        labelType.text = hoods[indexPath.row].type
        
        let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/images/"+hoods[indexPath.row].picture)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        image.image = UIImage(data: data!)
        return cell!
    }
    
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            if self.hoods[indexPath.row].user == self.username!
            {
                self.deleteHood(id: "\(self.hoods[indexPath.row].id)")
                self.hoods.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                let alertController = UIAlertController(title: "Succes", message:
                    "Annonce has been Deleted" + ", Thanks", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                let alertController = UIAlertController(title: "Fail", message:
                    "You cannot delete other annoncements" + ", Thanks", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
            
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    //    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    //        self.loadfromservice()
    //        tableView.reloadData()
    //        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toHoodDetails"
        {
            let indexPath = sender as! IndexPath
            let hood = hoods[indexPath.row]
            let dvc  = segue.destination as! DetailsHoodViewController
            dvc.titles = hood.title
            dvc.content = hood.content
            dvc.picture = hood.picture
            dvc.date = hood.date
            dvc.user = hood.user
            dvc.type = hood.type
            dvc.longitude = hood.longitude
            dvc.latitude = hood.lattitude
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { _, _ in
            
            if self.hoods[indexPath.row].user == "admin"
            {
                //self.deleteHood(id: self.hoods[indexPath.row].id as! String)
                self.hoods.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                
            }
            
            
        }
        deleteAction.backgroundColor = .red
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toHoodDetails", sender: indexPath)
        
    }
    
    @IBAction func onReload(_ sender: Any) {
        self.loadfromservice()
        tableView.reloadData();
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let defaults = UserDefaults.standard
               let  token : String?     = defaults.string(forKey: "token")
               print (token)
               do {
                   let jwt = try decode(jwt: token!)
                   let claim = jwt.claim(name: "username")
                   username = claim.string
                   print("Username in jwt was \(username)")
                   
               } catch let error as NSError {
                   error.localizedDescription
               }
        loadfromservice()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        tableView.addSubview(refreshControl)
        // not required when using UITableViewControlle
        
        // Do any additional setup after loading the view.
    }
    
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.longitude = location.coordinate.longitude
            self.lattitude = location.coordinate.latitude
            
            
            
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.loadfromservice()
        tableView.reloadData();
        
        DispatchQueue.main.async {
            self.tableView.refreshControl?.endRefreshing()
        }
    }
    
    
    func deleteHood(id:String) {
        
        
        
        //_ = UserDefaults.standard.set(data, forKey: "token") // FOR TOKEn
        guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/hood/"+id) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        // HTTPBODY for send request
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            do {
                
            } catch  {
                print (error)
            }
        }.resume()
        
    }
    
    func loadfromservice() {
        getUserLocation();
        let parameters = ["long": longitude, "latt": lattitude]
        print (parameters)
        
        //_ = UserDefaults.standard.set(data, forKey: "token") // FOR TOKEn
        guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/hood") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // HTTPBODY for send request
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([HoodEntity].self, from: d)
                    DispatchQueue.main.async {
                        self.hoods = decodedLists
                        print (self.hoods.count)
                        self.tableView.reloadData()
                        
                        
                    }
                }else {
                    print("No Data")
                }
            } catch  {
                print (error)
            }
        }.resume()
        
    }
}

func getValue(forKey key : String)->String? {
    return Bundle.main.infoDictionary?[key] as? String
}






