//
//  SettingsTableViewController.swift
//  JariIOS
//
//  Created by Sadok Laouissi on 6/28/20.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {


    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

  
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0 && indexPath.row == 0){
            // Logout
            print("Logout")
            
            let defaults = UserDefaults.standard
                      defaults.set( "", forKey: "token")
                      defaults.set( false, forKey: "isLogged")
                      
            
            self.performSegue(withIdentifier: "toLoginAfterLogout", sender: self)
            
            
        } else if(indexPath.section == 0 && indexPath.row == 1){
            // Change Password
            showAlertPassword()

        }

    }
   
    
    func showAlertPassword(){
        let alertController = UIAlertController(title: "Update Password", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
               textField.placeholder = "Current Password"
            textField?.isSecureTextEntry = true
           }
       
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
               let firstTextField = alertController.textFields![0] as UITextField
               let secondTextField = alertController.textFields![1] as UITextField
               let thirdTextField = alertController.textFields![2] as UITextField
           })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: {
               (action : UIAlertAction!) -> Void in })
        alertController.addTextField { (textField : UITextField!) -> Void in
               textField.placeholder = "New Password"
            textField?.isSecureTextEntry = true
           }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Confirm New Password"
            textField?.isSecureTextEntry = true
        }
           
           alertController.addAction(saveAction)
           alertController.addAction(cancelAction)
           
        self.present(alertController, animated: true, completion: nil)
    }
  
}
