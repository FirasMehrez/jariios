

import UIKit
import Mapbox
import Floaty

class LostViewController: UIViewController, MGLMapViewDelegate, FloatyDelegate{
    @IBOutlet weak var Search: UIImageView!
    
    var floatyQuad = Floaty()
    var losts = [LostEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadfromservice()
        let mapView = MGLMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.delegate = self
        
        // Set the map’s center coordinate and zoom level.
        mapView.setCenter(CLLocationCoordinate2D(latitude: 36.788054, longitude: 10.176320), zoomLevel: 13, animated: false)
        view.addSubview(mapView)
        //self.view.sendSubviewToBack(mapView)
        // Search.backgroundColor = UIColor.blue
        //  Search.layer.cornerRadius = Search.frame.height/2
        // Search.superview?.bringSubviewToFront(Search)
        
        
        // For QuadCircularAnimaition
        layoutFABforQuadAnimation(floaty: floatyQuad)
        floatyQuad.addDragging()
        floatyQuad.openAnimationType = .slideLeft // define animation type
        //floatyQuad.circleAnimationDegreeOffset = 10 // define offset in degrees
        // floatyQuad.circleAnimationRadius = 120 // by default is 150
        
    }
    
    
    func layoutFABforQuadAnimation(floaty : Floaty) {
        floaty.hasShadow = true
        floaty.paddingY = 100
        floaty.fabDelegate = self
        
        let item = FloatyItem()
        item.buttonColor = 	hexStringToUIColor(hex:"#D81B60")
        
        // hexStringToUIColor(hex:"#D81B60")
        floaty.addItem("Switch View", icon: UIImage(systemName: "repeat")){ item in
            
            self.performSegue(withIdentifier: "toListView", sender: self)
        }
        floaty.addItem("Add Lots & Found", icon: UIImage(systemName: "cart.fill.badge.plus")) { item in
            
            self.performSegue(withIdentifier: "toAddLostAndFound", sender: self)
        }
        // floaty.addItem(item: item)
        /*item in
         let alert = UIAlertController(title: "Hey", message: "I'm hungry...", preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "Me too", style: .default, handler: nil))
         self.present(alert, animated: true, completion: nil)*/
        self.view.addSubview(floaty)
    }
    func floatyWillOpen(_ floaty: Floaty) {
        print("Floaty Will Open")
    }
    
    
    func floatyDidOpen(_ floaty: Floaty) {
        print("Floaty Did Open")
    }
    
    func floatyWillClose(_ floaty: Floaty) {
        print("Floaty Will Close")
    }
    
    func floatyDidClose(_ floaty: Floaty) {
        print("Floaty Did Close")
    }
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        
        // Create point to represent where the symbol should be placed
        //let point = MGLPointAnnotation()
        /* point.coordinate = CLLocationCoordinate2D(latitude: 36.788054, longitude: 10.176320)
         point.title = "Hello world!"
         */
        var pointAnnotations = [MGLPointAnnotation]()
        DispatchQueue.main.async {
            
            self.loadfromservice()
            for i in 0 ..< self.losts.count {
                let point = MGLPointAnnotation()
                point.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(self.losts[i].lattitude), longitude:CLLocationDegrees(self.losts[i].longitude))
                
                
                point.title = "\(self.losts[i].name)"
                point.subtitle = "\(self.losts[i].name)"
                
                pointAnnotations.append(point)
               // mapView.selectAnnotation(point, animated: false, completionHandler: nil)
                print("\(self.losts[i].id)")
            }
            mapView.addAnnotations(pointAnnotations)
            
        }
        
        
        
        //mapView.selectAnnotation(point, animated: false, completionHandler: nil)
        
        //click
        
        
        
        
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
    // This example is only concerned with point annotations.
    guard annotation is MGLPointAnnotation else {
    return nil
    }
     
    // Use the point annotation’s longitude value (as a string) as the reuse identifier for its view.
    let reuseIdentifier = "\(annotation.coordinate.longitude)"
     
    // For better performance, always try to reuse existing annotations.
    var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
     
    // If there’s no reusable annotation view available, initialize a new one.
    if annotationView == nil {
    annotationView = CustomAnnotationView(reuseIdentifier: reuseIdentifier)
    annotationView!.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
     
    // Set the annotation view’s background color to a value determined by its longitude.
   // let hue = CGFloat(annotation.coordinate.longitude) / 100
        annotationView!.backgroundColor = hexStringToUIColor(hex: "#121143")
    }
     
    return annotationView
    }
     
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
    return true
    }
    
    
    
    func mapView(_ mapView: MGLMapView, calloutViewFor annotation: MGLAnnotation) -> MGLCalloutView? {
        // Instantiate and return our custom callout view.
        return CustomCalloutView(representedObject: annotation)
    }
    
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        // Optionally handle taps on the callout.
        print("Tapped the callout for: \(annotation)")
        
        // Hide the callout.
        mapView.deselectAnnotation(annotation, animated: true)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toListView"
        {
            let dvc  = segue.destination as! LostListViewController
            dvc.losts = losts
            
            
        }
    }
    
    
    
    func loadfromservice() {
        let parameters = ["long": "longitude", "latt": "lattitude"]
        
        
        //_ = UserDefaults.standard.set(data, forKey: "token") // FOR TOKEn
        guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/lost") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // HTTPBODY for send request
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([LostEntity].self, from: d)
                    DispatchQueue.main.async {
                        self.losts = decodedLists
                        print (self.losts.count)
                        //self.tableView.reloadData()
                        
                        
                    }
                }else {
                    print("No Data")
                }
            } catch  {
                print (error)
            }
        }.resume()
        
    }
}
// MGLAnnotationView subclass
class CustomAnnotationView: MGLAnnotationView {
    override func layoutSubviews() {
        super.layoutSubviews()
 
// Use CALayer’s corner radius to turn this view into a circle.
        layer.cornerRadius = bounds.width / 2
        layer.borderWidth = 2
        layer.borderColor = UIColor.white.cgColor
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
 
override func setSelected(_ selected: Bool, animated: Bool) {
super.setSelected(selected, animated: animated)
 
// Animate the border width in/out, creating an iris effect.
let animation = CABasicAnimation(keyPath: "borderWidth")
animation.duration = 0.1
layer.borderWidth = selected ? bounds.width / 4 : 2
layer.add(animation, forKey: "borderWidth")
}
}









