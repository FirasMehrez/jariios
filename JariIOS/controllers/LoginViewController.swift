//
//  LoginViewController.swift
//  JariIOS
//
//  Created by Firas mehrez on 23/06/2020.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import UIKit
import SwiftUI
import CoreData


class LoginViewController: UIViewController {
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        password.isSecureTextEntry = true
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func getValue(forKey key : String)->String? {
        return Bundle.main.infoDictionary?[key] as? String
    }
    @IBAction func onLoginAction(_ sender: Any) {
        
        
        
        
        
        if (userName.text == "" || password.text == "")
        {
            let alertController = UIAlertController(title: "Verify crediantials", message:
                "Empty fields", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
            
            self.present(alertController, animated: true, completion: nil)        }
        
        let parameters = ["username": userName.text!, "password": password.text!]
        
        guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/auth/login") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // HTTPBODY for send request
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            let response = response as! HTTPURLResponse
            let status = response.statusCode
            if status==200{
                
            }
            else {
                
                print("Error while login ")
                DispatchQueue.main.async {
                    
                    
                    let alertController = UIAlertController(title: "Verify crediantials", message:
                        "Username or password is incorrect", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
                return
            }
            
            guard let data = data else {
                
                return
            }
            print(String(data: data, encoding: .utf8)!)
            let defaults = UserDefaults.standard
            print(String(data: data, encoding: .utf8)!)
            defaults.set( String(data: data, encoding: .utf8)!, forKey: "token")
            defaults.set( true, forKey: "isLogged")
            
            
            // print(defaults.string(forKey: "token") as Any)
            
            
            //_ = UserDefaults.standard.set(data, forKey: "token")
            DispatchQueue.main.async {
                
                self.performSegue(withIdentifier: "LoginSegue", sender: self)
            }
            
            
        }.resume()
        
    }
    
}


