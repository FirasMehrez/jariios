//
//  AddHoodViewController.swift
//  JariIOS
//
//  Created by Firas mehrez on 27/06/2020.
//  Copyright © 2020 Firas mehrez. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Alamofire
import JSON
import JWTDecode

class AddHoodViewController: UIViewController,CLLocationManagerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var inputTitle: UITextField!
    @IBOutlet weak var inputContent: UITextField!
    @IBOutlet weak var inputType: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var locationMapView: MKMapView!
    
    var longitude:Double = 0.0
    var lattitude:Double = 0.0
    var imagePicker: UIImagePickerController!
    var   fileName2 = String();
    var url2: URL!
    enum ImageSource {
        case photoLibrary
        case camera
    }
    var username:String? = ""
    private var locationManager:CLLocationManager?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard
        let  token : String?     = defaults.string(forKey: "token")
        print (token)
        do {
            let jwt = try decode(jwt: token!)
            let claim = jwt.claim(name: "username")
            username = claim.string
            print("Username in jwt was \(username)")
            
        } catch let error as NSError {
            error.localizedDescription
        }
        getUserLocation();
        
        // Do any additional setup after loading the view.
    }
    func getValue(forKey key : String)->String? {
        return Bundle.main.infoDictionary?[key] as? String
    }
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        locationManager?.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.longitude = location.coordinate.longitude
            self.lattitude = location.coordinate.latitude
            let initialLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            locationMapView.centerToLocation    (initialLocation)
            
        }
    }
    
    
    
    
    
    
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - Saving Image here
    @IBAction func save(_ sender: AnyObject) {
        guard let selectedImage = imageView.image else {
            print("Image not found!")
            return
        }
        
        UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showAlertWith(title: "Save error", message: error.localizedDescription)
        } else {
            showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
        }
    }
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    func uploadImage(paramName: String, imageURL: URL){
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageURL, withName: "image")
            
        }, to: "http://"+getValue(forKey: "ipadress")!+":3000/upload")
            .uploadProgress { progress in
                print("Upload Progress: \(progress.fractionCompleted)")
        }
        .responseJSON { response in
            debugPrint(response)
        }
    }
    
    func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: UIImage, using boundary: String) -> Data {
        let data = NSMutableData()
        
        data.appendString("--\(boundary)\r\n")
        data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("Content-Type: \(mimeType)\r\n\r\n")
        data.append(fileData.jpegData(compressionQuality: 1.0)!)
        data.appendString("\r\n")
        
        return data as Data
    }
    
    
    
    
    
    
    @IBAction func selectImage(_ sender: UIButton) {
        
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
    
    @IBAction func addAction(_ sender: Any) {
        let parameters = ["title": inputTitle.text!,"type": inputType.text!,"username": username!,"picture": "Documents"+fileName2,"longitude": longitude,"latitude":lattitude ,"content":inputContent.text!] as [String : Any]
        
        
        //_ = UserDefaults.standard.set(data, forKey: "token") // FOR TOKEn
        uploadImage(paramName: inputTitle.text! , imageURL: url2)
        
        guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/hood/new") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // HTTPBODY for send request
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            let response = response as! HTTPURLResponse
            let status = response.statusCode
            if status==200{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Sucess", message:
                        "Hello " + ", Thanks for adding new Hood  ", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                    // for dismiss not sure if it works
                    //                        self.dismiss(animated: true, completion: {
                    //                            self.presentingViewController?.dismiss(animated: true, completion: nil)
                    //   })
                }
            }
            else {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Error", message:
                        "An error has been occured , please retry later", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                }
                print("Error while login ")
                return
            }
            guard let data = data else {
                return
            }
            print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async {
                
            }
        }.resume()
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}





private extension MKMapView {
    func centerToLocation(
        _ location: CLLocation,
        regionRadius: CLLocationDistance = 1000
    ) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}

extension AddHoodViewController{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
            let imgName = imgUrl.lastPathComponent
            fileName2 = imgName;
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let data = image.pngData()! as NSData
            data.write(toFile: localPath!, atomically: true)
            //let imageData = NSData(contentsOfFile: localPath!)!
            let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
            print(photoURL)
            url2 = photoURL
            
            guard let selectedImage = info[.originalImage] as? UIImage else {
                print("Image not found!")
                return
            }
            print(selectedImage)
            
            imageView.image = selectedImage
        }
    }
}
