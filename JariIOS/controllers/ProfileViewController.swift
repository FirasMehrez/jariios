

import UIKit
import JWTDecode
import MapKit


class ProfileViewController: UIViewController {
    
    var id : Int = 0
    var username : String = ""
    var email : String = ""
    var address: String = ""
    var phoneNumber: Int = 0
    var picture: String = ""
    var longitude : Double = 0.0
    var lattitude : Double = 0.0
    var birthDate: String = ""
    var createdAt : String = ""
    // MARK: - Properties
    
    @IBOutlet weak var bottomlabel: UILabel!
    @IBOutlet weak var bottomphone: UILabel!
    @IBOutlet weak var bottombirthdate: UILabel!
    @IBOutlet weak var map: MKMapView!
    
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .mainBlue
        
        view.addSubview(profileImageView)
        profileImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profileImageView.anchor(top: view.topAnchor, paddingTop: 80,
                                width: 120, height: 120)
        profileImageView.layer.cornerRadius = 120 / 2
        
//        view.addSubview(messageButton)
//        messageButton.anchor(top: view.topAnchor, left: view.leftAnchor,
//                             paddingTop: 64, paddingLeft: 32, width: 32, height: 32)
//
//        view.addSubview(followButton)
//        followButton.anchor(top: view.topAnchor, right: view.rightAnchor,
//                            paddingTop: 64, paddingRight: 32, width: 32, height: 32)
//
        view.addSubview(nameLabel)
        nameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nameLabel.anchor(top: profileImageView.bottomAnchor, paddingTop: 12)
        
        view.addSubview(emailLabel)
        emailLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailLabel.anchor(top: nameLabel.bottomAnchor, paddingTop: 4,paddingBottom: 20)
        
        
        return view
    }()
    
    
    
    let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.image =  #imageLiteral(resourceName: "venom")
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.borderWidth = 3
        iv.layer.borderColor = UIColor.white.cgColor
        return iv
    }()
    
//    let messageButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setImage( #imageLiteral(resourceName: "ic_mail_outline_white_2x").withRenderingMode(.alwaysOriginal), for: .normal)
//        button.addTarget(self, action: #selector(handleMessageUser), for: .touchUpInside)
//        return button
//    }()
//
//    let followButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setImage( #imageLiteral(resourceName: "baseline_person_add_white_36dp").withRenderingMode(.alwaysOriginal), for: .normal)
//        button.addTarget(self, action: #selector(handleFollowUser), for: .touchUpInside)
//        return button
//    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 26)
        label.textColor = .white
        return label
    }()
    
    let emailLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .white
        return label
    }()
    
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        load()
        
        view.backgroundColor = .white
        
        view.addSubview(containerView)
        containerView.anchor(top: view.topAnchor, left: view.leftAnchor,
                             right: view.rightAnchor, height: 300)
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Selectors
    
    @objc func handleMessageUser() {
        print("Message user here..")
    }
    
    @objc func handleFollowUser() {
        print("Follow user here..")
        //s  self.present(EditProfileViewController(), animated: true, completion: nil)
        
        
    }
    
    func load() {
        let defaults = UserDefaults.standard
        var username:String? = ""
        
        let  token : String?     = defaults.string(forKey: "token")
        print (token)
        do {
            let jwt = try decode(jwt: token!)
            let claim = jwt.claim(name: "username")
            username = claim.string
            print("Username in jwt was \(username)")
            
        } catch let error as NSError {
            error.localizedDescription
        }
        
        
        let parameters = ["username": username!] as [String : Any]
        
        //_ = UserDefaults.standard.set(data, forKey: "token") // FOR TOKEn
        guard let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/user/me") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // HTTPBODY for send request
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            let parseResult: [String:AnyObject]!
            do {
                
                parseResult = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                DispatchQueue.main.async {
                    
                    
                    self.id = parseResult["id"]! as! Int
                    self.username = parseResult["username"]! as! String
                    self.email = parseResult["email"]! as! String
                    self.phoneNumber = parseResult["phoneNumber"]! as! Int
                    self.address = parseResult["address"]! as! String
                    self.birthDate = parseResult["birthDate"]! as! String
                    self.createdAt = parseResult["createdAt"]! as! String
                    self.picture = parseResult["picture"]! as! String
                    
                    self.emailLabel.text = self.email
                    self.nameLabel.text = self.username
                    self.bottomlabel.text =  "\(self.username)"
                    self.bottomphone.text = "\(self.phoneNumber)"
                    
                    //                    self.labeladdress.text =  "\(self.address)"
                    let index = self.birthDate.index(self.birthDate.startIndex, offsetBy: 10)
                    let datedate = self.birthDate[..<index]
                    print(datedate)
                    self.bottombirthdate.text = String(datedate)
                    
                    let initialLocation = CLLocation(latitude: CLLocationDegrees(self.lattitude), longitude: CLLocationDegrees(self.longitude))
                    self.map.centerToLocation    (initialLocation)
                    
                    let url = URL(string: "http://"+getValue(forKey: "ipadress")!+":3000/images/"+self.picture)
                                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                      self.profileImageView.image = UIImage(data: data!)
                    _ = try? Data(contentsOf: url!)
                    
                }
                
            } catch  {
                print (error)
            }
        }.resume()
        
    }
    
    
}

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static let mainBlue = UIColor.rgb(red: 3, green: 13, blue: 94)
}

extension UIView {
    
    func anchor(top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, paddingTop: CGFloat? = 0,
                paddingLeft: CGFloat? = 0, paddingBottom: CGFloat? = 0, paddingRight: CGFloat? = 0, width: CGFloat? = nil, height: CGFloat? = nil) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop!).isActive = true
        }
        
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft!).isActive = true
        }
        
        if let bottom = bottom {
            if let paddingBottom = paddingBottom {
                bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
            }
        }
        
        if let right = right {
            if let paddingRight = paddingRight {
                rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
            }
        }
        
        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}

private extension MKMapView {
    func centerToLocation(
        _ location: CLLocation,
        regionRadius: CLLocationDistance = 1000
    ) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}
