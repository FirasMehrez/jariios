//
//  TabBarController.swift
//  
//
//  Created by Firas mehrez on 26/06/2020.
//

import Foundation

import UIKit
import SwiftUI



struct SecondView: View {
  var body: some View {
      VStack {
          Text("Second View").font(.system(size: 36))
          Text("Loaded by SecondView").font(.system(size: 14))
      }
  }
}
class TabBarController: UIHostingController<SecondView> {



   class TabBarHostViewController: UIHostingController<SecondView> {
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder, rootView: SecondView())
       }
   }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */



struct TabBarHostViewController_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
